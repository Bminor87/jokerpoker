<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'CardController@index');
Route::post('/cards', 'CardController@index');
Route::get('/cards/reset', 'CardController@reset');

Route::post('/cards/start', 'CardController@start');
Route::post('/cards/hold', 'CardController@hold');

Route::post('/cards/double', 'CardController@double');
Route::post('/cards/double/result', 'CardController@doubleResult');

Route::post('/cards/collect', 'CardController@collect');