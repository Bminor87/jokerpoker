<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Cards;
use App\Http\Requests;

class CardController extends Controller {

  public $cards;


  public function __construct()
  {
    $this->cards = new Cards();
    
    if(!session()->has('money')) session()->put('money', 1000);
  }

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index($winning = 0)
  {
    return view('start', compact('winning'))->with('money', session()->get('money'));
  }

  public function start(Request $request)
  {
    session()->put('bet', $request->bet);
    $bet = session()->get('bet');
    session()->put('last_bet', $bet);

    if (session()->get('money') - $bet < 0 || $bet < 1)
    {
      $message = ($bet < 1) ? 'Cannot bet below 1' : 'Insufficient Funds!';

      return view('start', compact('message'))->with('money', session()->get('money'));
    }

    $request->session()->put('money', session()->get('money') - $bet);

    $result = $this->cards->play();

    $player1 = $this->cards->player1;

    return view('first', compact('result', 'player1', 'bet'))->with('money', session()->get('money'));
  }

  public function hold(Request $request)
  {
    $bet = session()->get('bet');

    $holded = $request->holded ? $request->holded : [];

    $result = $this->cards->play($holded);

    $player1 = $this->cards->player1;

    return view('second', compact('result', 'player1', 'bet', 'holded'))->with('money', session()->get('money'));
  }

  public function double(Request $request)
  {
    $bet = session()->get('bet');

    $prize = $request->money;

    return view('double', compact('bet', 'prize'))->with('money', session()->get('money'));
  }

  public function doubleResult(Request $request)
  {
    $bet = session()->get('bet');

    $prize = $request->money;

    $card = $this->cards->double();

    switch ($request->double_type)
    {
      case 'small':
        $result = ($card['value'] < 7 || $card['value'] == 15) ? true : false;
        break;

      case 'big':
        $result = ($card['value'] > 7) ? true : false;
        break;

      default:
        $result = false;
    }

    if ( ! $result) $prize = 0;

    $card = $card['card'];

    return view('double_result', compact('result', 'card', 'bet', 'prize'))->with('money', session()->get('money'));
  }

  public function reset()
  {
    session()->put('money', 1000);

    return $this->index();
  }

  public function collect(Request $request)
  {
    $prize = $request->money;

    session()->put('money', session()->get('money') + $prize);

    return $this->index($prize);
  }
}
