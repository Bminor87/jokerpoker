<?php namespace App\Repositories;

class Cards {

  public $player1;

  public $wins;

  public $prizes = [
    1  => 0,
    2  => 1,
    3  => 2,
    4  => 3,
    5  => 5,
    6  => 8,
    7  => 10,
    8  => 25,
    9  => 60,
    10 => 100,
    11 => 150
  ];

  protected $deck = [];

  protected $values = [
    '0' => 0,
    '1' => 1,
    '2' => 2,
    '3' => 3,
    '4' => 4,
    '5' => 5,
    '6' => 6,
    '7' => 7,
    '8' => 8,
    '9' => 9,
    'T' => 10,
    'J' => 11,
    'Q' => 12,
    'K' => 13,
    'A' => 14,
    'X' => 15
  ];

  protected $hands = [
    1  => 'High Card',
    2  => 'Pair',
    3  => 'Two Pairs',
    4  => 'Three of a kind',
    5  => 'Straight',
    6  => 'Flush',
    7  => 'Full House',
    8  => 'Four of a kind',
    9  => 'Five of a kind',
    10 => 'Straight Flush',
    11 => 'Royal Flush'
  ];

  /**
   * @desc Instantiate Card class by making the deck
   *
   * @param int $jokers Number of jokers
   */
  public function __construct($jokers = 2)
  {
    $this->reset_deck($jokers);
  }

  /**
   * @desc Start the game! Deal, and Show results!
   *
   * @param array $holded If any cards are holded, they will be included
   *
   * @return bool
   */
  public function play($holded = [])
  {

    $this->player1['cards'] = $this->deal($holded);

    /*
     * Uncomment the following if, for testing purposes,
     * you want the dealer to deal certain cards
     */

    //$this->player1['cards'] = ['5D', 'AD', '4D', 'XR', 'XB'];

    //$this->player1['cards'] = ['QD', '9D', 'XB', 'XR', 'AD'];

    return $this->check_if_player_wins();
  }

  /**
   * @desc Give a random card for the doubling game
   *
   * @return array A random card and its value only (separately)
   */
  public function double()
  {
    $this->shuffle();
    $card = $this->deck[mt_rand(0, 53)];
    $value = $this->values_only($card);
    if ( $value == 14 ) $value = 1;

    return compact('card', 'value');
  }

  /**
   * @desc Check which hand the player has
   *
   * @param $hand array The player's hand
   *
   * @return int The hand number in accordance with $this->hands
   */
  protected function hand_score($hand)
  {
    if (
	    ($r = $this->royalflush($hand)) ||
      ($r = $this->straightflush($hand)) ||
      ($r = $this->five($hand)) ||
      ($r = $this->four($hand)) ||
      ($r = $this->fullhouse($hand)) ||
      ($r = $this->flush($hand)) ||
      ($r = $this->straight($hand)) ||
      ($r = $this->three($hand)) ||
      ($r = $this->twopairs($hand)) ||
      ($r = $this->pair($hand))
    )
    {
      return $r;
    }

  }

  /**
   * @desc Checks if the hand has a pair
   *
   * @param $hand
   *
   * @return int 2 if pair 1 if not (Only High Card)
   */
  protected function pair($hand)
  {
    $pairs = $this->apply_jokers($this->values_only($hand));

    return (in_array(2, $pairs)) ? 2 : 1;
  }

  /**
   * @desc Checks if the hand has two pairs
   *
   * @param $hand
   *
   * @return bool|int 3 if two pairs, false if not
   */
  protected function twopairs($hand)
  {
    $pairs = $this->apply_jokers($this->values_only($hand));
    $pairs[array_search(2, $pairs)] = 0;
    return (in_array(2, $pairs)) ? 3 : false;
  }

  /**
   * @desc Checks if the hand has three of a kind
   *
   * @param $hand
   *
   * @return bool|int 4 if three of a kind, false if not
   */
  protected function three($hand)
  {
    $pairs = $this->apply_jokers($this->values_only($hand));

    return (in_array(3, $pairs)) ? 4 : false;
  }

  /**
   * @desc Checks if the hand is straight
   *
   * @param $hand
   *
   * @return bool|int 5 if straight, false if not
   */
  protected function straight($hand)
  {
    $hand = $this->values_only($hand);

    $hand = $this->filter_ace_value($hand);

    $amount_of_jokers = $this->get_pairs($hand)[15];
    
    // old version

    sort($hand);

    $i = 0;
    while($amount_of_jokers)
    {
      if ($i > 3) {
        break; // This was necessary in case of QD, AD, XB, XR, KD, but don't know why
      }
      if ($hand[$i] != $hand[$i+1] - 1) {
        $amount_of_jokers -= 1;
        $hand[array_search(15, $hand)] = $hand[$i] + 1;
        sort($hand);
      }
      $i++;
    }

    sort($hand);

    if ($hand[0] == $hand[1] - 1 &&
        $hand[0] == $hand[2] - 2 &&
        $hand[0] == $hand[3] - 3 &&
        $hand[0] == $hand[4] - 4)
        {
          return 5;
        }
        else
        {
          return false;
        }
  }

  /**
   * @desc Checks if the hand has a flush
   *
   * @param $hand
   *
   * @return bool|int 6 if flush, false if not
   */
  protected function flush($hand)
  {
    return (count(array_unique($this->shapes_only($hand))) == 1) ? 6 : false;
  }

  /**
   * @desc Checks if the hand has a full house
   *
   * @param $hand
   *
   * @return bool|int 7 if full house, false if not
   */
  protected function fullhouse($hand)
  {
    $pairs = $this->apply_jokers($this->values_only($hand));

    return (in_array(3, $pairs) && in_array(2, $pairs)) ? 7 : false;
  }

  /**
   * @desc Checks if the hand has four of a kind
   *
   * @param $hand
   *
   * @return bool|int 8 if four, false if not
   */
  protected function four($hand)
  {
    $pairs = $this->apply_jokers($this->values_only($hand));

    return (in_array(4, $pairs)) ? 8 : false;
  }

  /**
   * @desc Checks if the hand has five of a kind
   *
   * @param $hand
   *
   * @return bool|int 9 if five, false if not
   */
  protected function five($hand)
  {
    $pairs = $this->apply_jokers($this->values_only($hand));

    return (in_array(5, $pairs)) ? 9 : false;
  }

  /**
   * @desc Checks if the hand has a straight flush
   *
   * @param $hand
   *
   * @return bool|int 10 if straight flush, false if not
   */
  protected function straightflush($hand)
  {
    return ($this->straight($hand) && $this->flush($hand)) ? 10 : false;
  }

  /**
   * @desc Checks if the hand has a royal straight flush
   *
   * @param $hand
   *
   * @return bool|int 11 if royal straight flush, false if not
   */
  protected function royalflush($hand)
  {
  	$values = $this->values_only($hand);
  	sort($values);
	  
    return ($this->straightflush($hand) && ($values[0] >= 10)) ? 11 : false;
  }

  /**
   * @desc Make an array where the value and the shape of
   *       each card are separated
   *
   * @param $hand
   *
   * @return array
   */
  private function separate_values_and_shapes($hand)
  {
    foreach ($hand as $card)
    {
      $array[] = ['value' => $this->values[substr($card, 0, 1)],
                  'shape' => substr($card, 1, 1)];
    }

    return $array;
  }

  /**
   * @desc Get the values of each card only
   *
   * @param $hand
   *
   * @return array
   */
  private function values_only($hand)
  {
    if ( ! is_array($hand) ) return $this->values[substr($hand, 0, 1)];

    foreach ($hand as $card) $values[] = $card['value'];
    rsort($values);

    return $values;
  }

  /**
   * @desc Get the shapes of each card only
   *
   * @param $hand
   *
   * @return array
   */
  private function shapes_only($hand)
  {
    foreach ($hand as $card) if ( $card['shape'] != 'R' && $card['shape'] != 'B' ) $shapes[] = $card['shape'];

    return $shapes;
  }

  /**
   * @desc Apply Jokers to the hand containing pairs
   *
   * @param $hand
   *
   * @return array
   */
  private function apply_jokers($hand)
  {
    $pairs = $this->get_pairs($hand);

    $amount_of_jokers = $pairs[15];
    $pairs[15] = 0; // unset Jokers

    $pairs_rev = array_reverse($pairs);

    $highest_two_pos = array_search(2, $pairs_rev);
    $highest_one_pos = array_search(1, $pairs_rev);

    $highest = $highest_two_pos ? 15 - $highest_two_pos : 15 - $highest_one_pos;

    if ( $highest_three_pos = array_search(3, $pairs_rev) ) $highest = 15 - $highest_three_pos;

    if ( $highest_four_pos = array_search(4, $pairs_rev) ) $highest = 15 - $highest_four_pos;

    $pairs[$highest] += $amount_of_jokers;

    return $pairs;
  }

  /**
   * @desc Checks if Player has a hand good enough for win.
   *       Also sets (yeah yeah it's not exactly SOLID) the
   *       hand_id, prize, hand and separated arrays for
   *       the player variable.
   *
   * @return bool Win or Not win
   */
  private function check_if_player_wins()
  {
    $this->player1['separated'] = $this->separate_values_and_shapes($this->player1['cards']);

    $hand_score = $this->hand_score($this->player1['separated']);

    $this->player1['hand_id'] = $hand_score;

    $this->player1['prize'] = $this->prizes[$hand_score];

    $this->player1['hand'] = $this->hands[$hand_score];

    // Must be at least a pair to win
    return ($hand_score > 1);
  }

  /**
   * @desc Deals new cards to the player
   *
   * @param $holded array Holded cards will be included to the hand
   *
   * @return array
   */
  private function deal($holded)
  {
    // TODO: MAKE IT SO IT DOESN'T DEAL THE SAME CARDS THAT WERE DEALT AT FIRST BUT NOT HOLDED!

    $this->shuffle($holded);

    $newHand = [];
    for ($i = 0; $i < 5; $i ++)
    {
      $newHand[$i] = array_key_exists($i, $holded) ? $holded[$i] : $this->deck[$i];
    }

    ksort($newHand);

    return $newHand;
  }

  /**
   * @desc Shuffles the Deck
   *
   * @param bool|array $holded Leaves out the holded cards from the deck
   */
  private function shuffle($holded = false)
  {
    if ( $holded )
    {
      foreach ($holded as $card)
      {
        unset($this->deck[array_search($card, $this->deck)]);
      }
    }

    shuffle($this->deck);
  }

  /**
   * @desc Initiates the deck. Accepts the amount of desired
   *       jokers as parameter.
   *
   * @param $jokers
   */
  private function reset_deck($jokers)
  {
    $values = array_flip($this->values);
    for ($i = 2; $i < 15; $i ++)
    {
      $this->deck[] = $values[$i] . 'C';
      $this->deck[] = $values[$i] . 'D';
      $this->deck[] = $values[$i] . 'H';
      $this->deck[] = $values[$i] . 'S';
    }

    while ($jokers)
    {
      $jokers % 2 ? $this->deck[] = 'XR' : $this->deck[] = 'XB';
      $jokers --;
    }
  }

  /**
   * @desc makes an array where each card-value is the key,
   *        and the amount of cards of that value in the hand
   *       is the value.
   *
   * @param $hand
   *
   * @return array
   */
  private function get_pairs($hand)
  {
    $pairs = array_fill_keys($this->values, 0);

    foreach ($hand as $card) $pairs[$card] += 1;

    return $pairs;
  }

  /**
   * @desc Filters the first found Ace for a straight,
   *       so it can be used as a one in the smallest
   *       possible straight
   *
   * @param $hand
   *
   * @return mixed
   */
  private function filter_ace_value($hand)
  {
    sort($hand);
    if ($hand[0] < 5) 
    {
      $hand[array_search(14, $hand)] = 1;
    }
    sort($hand);
    return $hand;
  }

}