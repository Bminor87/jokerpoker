<!DOCTYPE html>
<html>
<head>
    <title>Laravel</title>

    <link href="//fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
    <link href="{{ elixir('css/app.css') }}" rel="stylesheet"/>

</head>
<body>
<div class="container">
    <div class="content text-center">
        @yield('form')
        <h1>Joker Poker</h1>

        <h2>Money: $ <span class="money_hud {{ $money ? 'text-success' : 'text-danger' }}">{{ isset($winning) ? $money - $winning : number_format($money, 0,'.',',') }}</span></h2>
        <div class="row text-center"><a href="/cards/reset">Reset Game</a></div>

        <div class="row text-center">

                @yield('content')

        </div>

        @yield('bottom')
        {!! Form::close() !!}
    </div>
</div>

<script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
@yield('scripts')

</body>
</html>