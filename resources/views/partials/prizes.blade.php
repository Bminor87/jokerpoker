<div class="row">
    <table class="prizes">
        <tr><td class="text-center" colspan="2"><b>Bet:</b> $ {{ number_format($bet,0,'.',',') }}</td></tr>
        <tr id="hand_2">
            <td>Pair: </td><td>$ {{ number_format($bet,0,'.',',') }}</td>
        </tr>
        <tr id="hand_3">
            <td>Two Pairs: </td><td>$ {{ number_format($bet * 2,0,'.',',') }}</td>
        </tr>
        <tr id="hand_4">
            <td>Three of a kind: </td><td>$ {{ number_format($bet * 3,0,'.',',') }}</td>
        </tr>
        <tr id="hand_5">
            <td>Straight: </td><td>$ {{ number_format($bet * 5,0,'.',',') }}</td>
        </tr>
        <tr id="hand_6">
            <td>Flush: </td><td>$ {{ number_format($bet * 8,0,'.',',') }}</td>
        </tr>
        <tr id="hand_7">
        <td>Full House: </td><td>$ {{ number_format($bet * 10,0,'.',',') }}</td>
        </tr>
        <tr id="hand_8">
            <td>Four of a kind: </td><td>$ {{ number_format($bet * 25,0,'.',',') }}</td>
        </tr>
        <tr id="hand_9">
            <td>Five of a kind: </td><td>$ {{ number_format($bet * 60,0,'.',',') }}</td>
        </tr>
        <tr id="hand_10">
            <td>Straight Flush: </td><td>$ {{ number_format($bet * 100,0,'.',',') }}</td>
        </tr>
        <tr id="hand_11">
            <td>Royal Flush: </td><td>$ {{ number_format($bet * 150,0,'.',',') }}</td>
        </tr>
    </table>
</div>