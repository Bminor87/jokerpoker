@extends('cards')

@section('form')
    {!! Form::open(['action' => 'CardController@double', 'class' => 'form-horizontal']) !!}
@endsection

@section('content')
    @include('partials.prizes')
    <div class="col-md-12 deck text-center">
        <div class="row col-sm-12">
            <div class="col-xs-1">&nbsp;</div>
            <?php $i = 0 ?>
            @foreach ($player1['cards'] as $card)
                <figure class="col-xs-2 {{ ( ! is_null($holded) && in_array($card, $holded)) ? 'holded' : 'figure' }}">
                    <img class="{{ ( ! is_null($holded) && in_array($card, $holded)) ? 'holded' : 'card' }}"
                         src="/img/cards/{{ $card }}.png"></img>
                </figure>
            @endforeach
            <div class="col-xs-1">&nbsp;</div>
        </div>
        @if(isset($player1))
            <h4 class="hand">{{ $player1['hand'] }}</h4>
        @endif
    </div>

@endsection

@section('bottom')
    <h2 class="row text-center result">
        @if($result)
            <span class="text-success">You won $ {{ number_format($bet * $player1['prize'],0,'.',',') }} with {{ $player1['hand'] }}!!</span>
        @else
            <span class="text-danger">Sorry, no win this time!</span>
        @endif
    </h2>
    @if($result)
        <div class="row text-center result">
            <input type="hidden" name="money" value="{{ $bet * $player1['prize'] }}"/>
            <input type="submit" class="btn btn-lg btn-warning" value="Double"/>
        </div>
    @endif
    {!! Form::close() !!}
    {!! Form::open(['action' => 'CardController@collect', 'class' => 'form-horizontal']) !!}
    <div class="row text-center">
        <input type="hidden" name="money" value="{{ $bet * $player1['prize'] }}"/>
        <input type="submit" class="btn btn-lg btn-danger" value="New Game"/>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function () {

            var showCards = function () {
                setTimeout(function () {

                    if (typeof nextCard === 'undefined') {
                        if (typeof firstCard === 'undefined') {
                            var firstCard = $('.figure').first();
                            firstCard.children('.card').show('drop', {direction: 'up'}, 500);
                            nextCard = firstCard.next();
                        }
                    }

                    var delay = 500;
                    while (nextCard.next().next().length !== 0)
                    {
                        if(!nextCard.hasClass('holded'))
                        {

                            nextCard.children('.card').delay(delay).show('drop', {direction: 'up'}, 500);
                            delay = delay + 500;
                        }

                        nextCard = nextCard.next();
                    }

                    nextCard.children('.card').delay(delay).show('drop', {direction: 'up'}, 1500);
                    $('.hand').delay(delay + 500).slideDown();
                    $('.result').delay(delay + 700).fadeIn(function () {
                        $('#hand_{{ $player1['hand_id'] }}').css('background', 'rgb(60,118,61)');
                    });

                }, 100);
            }
            showCards();
        });
    </script>
@endsection