@extends('cards')

@section('form')
    {!! Form::open(['action' => 'CardController@hold', 'class' => 'form-horizontal']) !!}
@endsection

@section('content')
    @include('partials.prizes')
    <div class="col-md-12 deck text-center">
        <div class="row col-sm-12">
            <div class="col-xs-1">&nbsp;</div>
            <?php $i = 0 ?>
            @foreach ($player1['cards'] as $card)
                <figure class="figure col-xs-2">
                    <span class="hold">HOLD</span>
                    <img class="card" src="/img/cards/{{ $card }}.png"></img>
                    {!! Form::checkbox('holded['.$i++.']', $card, false) !!}
                </figure>
            @endforeach
            <div class="col-xs-1">&nbsp;</div>
        </div>
        <h4>Click the cards you want to hold</h4>
    </div>
@endsection

@section('bottom')
    <div class="row text-center result">
        <input type="submit" class="btn btn-lg btn-danger" value="Deal"/>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function(){
            $('.figure').children('.card').click(function () {
                $(this).next('input[type=checkbox]').trigger('click');
                $(this).prev('.hold').toggle();
            });


            setTimeout(function(){
                var firstCard = $('.figure').first();
                var secondCard = firstCard.next();
                var thirdCard = secondCard.next();
                var fourthCard = thirdCard.next();
                var fifthCard = fourthCard.next();
                firstCard.children('.card').show('drop', {direction: 'up'}, 500);
                secondCard.children('.card').delay(500).show('drop', {direction: 'up'}, 500);
                thirdCard.children('.card').delay(1000).show('drop', {direction: 'up'}, 500);
                fourthCard.children('.card').delay(1500).show('drop', {direction: 'up'}, 500);
                fifthCard.children('.card').delay(2000).show('drop', {direction: 'up'}, 1500);

                //$('input[type=checkbox]').delay(2500).fadeIn();
                $('.hand').delay(2800).slideDown();
                $('.result').delay(3000).fadeIn();

            }, 500);
        });
    </script>
@endsection