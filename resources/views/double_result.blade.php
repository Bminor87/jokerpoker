@extends('cards')

@section('form')
    {!! Form::open(['action' => 'CardController@double', 'class' => 'form-horizontal']) !!}
@endsection

@section('content')
    @include('partials.prizes')
    <div class="col-md-12 deck text-center">
        <div class="row col-sm-12">
            <div class="col-xs-5">&nbsp;</div>
            <figure class="col-xs-2 figure">
                <img class="card" src="/img/cards/{{ $card }}.png"></img>
            </figure>
            <div class="col-xs-5">&nbsp;</div>
        </div>
        <h4 class="result">{{ $result ? 'Win' : 'No Win' }}</h4>
    </div>

@endsection

@section('bottom')
    <h2 class="row text-center result">
        @if($result)
            <span class="text-success">You won {{ number_format($prize * 2,0,'.',',') }} in total!</span>
        @else
            <span class="text-danger">Sorry, you lost!</span>
        @endif
    </h2>
    @if($result)
        <div class="row text-center result">
            <input type="hidden" name="money" value="{{ $prize * 2 }}"/>
            <input type="submit" class="btn btn-lg btn-warning" value="Double"/>
        </div>
    @endif
    {!! Form::close() !!}
    {!! Form::open(['action' => 'CardController@collect', 'class' => 'form-horizontal']) !!}
    <div class="row text-center">
        <input type="hidden" name="money" value="{{ $prize * 2 }}"/>
        <input type="submit" class="btn btn-lg btn-danger" value="New Game" />
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function () {
            setTimeout(function(){
                $('.figure').children('.card').show('drop', {direction: 'up'}, 1500);
                $('.result').delay(1500).fadeIn();
            },1500);
        });
    </script>
@endsection