@extends('cards')

@section('form')

@endsection

@section('content')
    @include('partials.prizes')
    <div class="col-md-12 deck text-center">
        <div class="row col-sm-12">
            <div class="col-xs-5">&nbsp;</div>
            <figure class="col-xs-2">
                <img src="/img/cards/back.png"></img>
            </figure>
            <div class="col-xs-5">&nbsp;</div>
        </div>
        <h4>&nbsp;</h4>
    </div>

@endsection

@section('bottom')
    <div class="row text-center">
        <h4>Current prize: $ <span class="text-info">{{ number_format($prize,0,'.',',') }}</span></h4>
        <h4>To win: $ <span class="text-success">{{ number_format($prize * 2,0,'.',',') }}</span></h4>
    </div>
    <div class="row text-center result">
        {!! Form::open(['action' => 'CardController@doubleResult', 'class' => 'form-horizontal']) !!}
        <div class="col-xs-6">
            <input type="hidden" name="double_type" value="small"/>
            <input type="hidden" name="money" value="{{ $prize }}"/>
            <input type="submit" class="btn btn-lg btn-warning" value="Small"/>
            <h3>A - 6</h3>
        </div>
        {!! Form::close() !!}
        {!! Form::open(['action' => 'CardController@doubleResult', 'class' => 'form-horizontal']) !!}
        <div class="col-xs-6">
            <input type="hidden" name="double_type" value="big"/>
            <input type="hidden" name="money" value="{{ $prize }}"/>
            <input type="submit" class="btn btn-lg btn-warning" value="Big"/>
            <h3>8 - K</h3>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function () {
            $('.result').delay(500).fadeIn();
        });
    </script>
@endsection