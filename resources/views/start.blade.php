@extends('cards')

@section('form')
    {!! Form::open(['action' => 'CardController@start', 'class' => 'form-horizontal']) !!}
@endsection

@section('content')
    <div class="col-md-12 deck text-center">
        <div class="row col-sm-12">
            <div class="col-xs-1">&nbsp;</div>
            @foreach(range(1,5) as $i)
                <figure class="col-xs-2">
                    <img src="/img/cards/back.png"></img>
                </figure>
            @endforeach
            <div class="col-xs-1">&nbsp;</div>
        </div>
        <h4>&nbsp;</h4>
    </div>
@endsection

@section('bottom')
    <div class="row text-center">
        <!-- Bet Form Input -->
        <fieldset class="form-group">
            {!! Form::label('bet', 'Bet:', ['class' => 'control-label']) !!}
            <input type="number" value="{{ session()->get('last_bet', 100) }}" min="1" name="bet"/>
            @if (isset($message))
                <h1 class="text-danger">{{ $message }}</h1>
            @endif
        </fieldset>
        <input type="submit" class="btn btn-lg btn-danger" value="Start"/>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function(){
            var count = 0;
            var CurrentMoney =  parseInt($('.money_hud').text());
            var MoneyRoll = function(){
                setTimeout(function(){
                    if (count < {{ isset($winning) ? $winning : 0 }}) {
                        CurrentMoney = CurrentMoney + {{ isset($winning) ? $winning / 100 : 1 }};
                        $('.money_hud').text(Math.floor(CurrentMoney));
                        count = count + {{ isset($winning) ? $winning / 100 : 1 }};
                        MoneyRoll();
                    }
                    else {
                        $('.money_hud').text("{{ number_format($money, 0, '.', ',') }}");
                    }
                },15);
            };
            MoneyRoll();
        });
    </script>
@endsection